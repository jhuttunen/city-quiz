'''file used to import csv -file to SQLite database'''

from pathlib import Path
p = str(Path(__file__).parent.resolve())
pdb = p + '/cities.db'
Path(pdb).touch()

import sqlite3
conn = sqlite3.connect(p + '/cities.db')
c = conn.cursor()
# create table in the db
c.execute('''CREATE TABLE cities (city,city_ascii,lat,lng,country,iso2,iso3,admin_name,capital,population,id)''')

import pandas as pd
# load the data into a Pandas DataFrame
users = pd.read_csv(p + '/worldcities.csv')
# write the data to a sqlite table
users.to_sql('cities', conn, if_exists='append', index = False)

#c.execute('''SELECT * FROM cities''').fetchall() 