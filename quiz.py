import random
from math import radians, cos, sin, asin, sqrt
from database import Database

class Quiz:
    '''Class to handle the quiz'''

    def __init__(self, config):
        self.round = 1
        self.score = 0
        self.wrong_answers = 0
        self.questions_list = []
        self.config = config

    def new_question(self, q_type=""):
        '''Method to generate question'''

        # ask capital only if all countries in the settings
        if(self.config.selected_countries == "all"):
            q_types = ["distances", "capital", "population", "highest_population"]
        else:
            q_types = ["distances", "population", "highest_population"]

        # if question type not specified, use random
        if(q_type == ""):
            q_type = random.choice(q_types)

        question = Question(q_type, "", "", "")
        db = Database()

        # question type 1: ask which city is x distance from three other cities
        if(q_type == "distances"):

            q_data = db.get_random_cities(7, q_type, self.config)

            # row 0 is the answer
            question.answer = f"{q_data[0][0]} ({q_data[0][4]})"

            # rows 1 to 3 are other options
            question.options = []
            for i in range(4):
                question.options.append(f"{q_data[i][0]} ({q_data[i][4]})")            

            # coordinates for the answer
            loc1=(q_data[0][2],q_data[0][3])

            # calculate distances from answer location to three other cities
            dist4=self.getDistanceFromLatLng(q_data[0][2], q_data[0][3], q_data[4][2], q_data[4][3])
            dist5=self.getDistanceFromLatLng(q_data[0][2], q_data[0][3], q_data[5][2], q_data[5][3])
            dist6=self.getDistanceFromLatLng(q_data[0][2], q_data[0][3], q_data[6][2], q_data[6][3])
            
            question.text = "What city is located\n" 
            question.text += "{:.0f}km from {} ({})\n".format(dist4, q_data[4][0], q_data[4][4])
            question.text += "{:.0f}km from {} ({})\n".format(dist5, q_data[5][0], q_data[5][4])
            question.text += "{:.0f}km from {} ({})\n".format(dist6, q_data[6][0], q_data[6][4])

        # question type 2: ask which is the capital of country x
        elif(q_type == "capital"):

            q_data = db.get_random_cities(4, q_type, self.config)

            # row 0 is the answer
            question.answer = f"{q_data[0][0]}"

            # rows 1 to 3 are other options
            question.options = []
            for i in range(4):
                question.options.append(f"{q_data[i][0]}")     
            
            question.text = f"What is the capital of {q_data[0][4]}? \n"

        # question type 3: ask what is the population of city x
        elif(q_type == "population"):

            q_data = db.get_random_cities(4, q_type, self.config)

            # row 0 is the answer
            question.answer = f"{int(q_data[0][9]):,d}".replace(","," ")

            # rows 1 to 3 are other options
            question.options = []
            for i in range(4):
                question.options.append(f"{int(q_data[i][9]):,d}".replace(","," "))          
            
            question.text = f"What is the population of {q_data[0][0]} ({q_data[0][4]})? \n"

        # question type 4: ask which of cities has highest population
        elif(q_type == "highest_population"):

            q_data = db.get_random_cities(4, q_type, self.config)
            
            cities = [q_data[0][0], q_data[1][0], q_data[2][0], q_data[3][0]]
            pops = [int(q_data[0][9]), int(q_data[1][9]), int(q_data[2][9]), int(q_data[3][9])]
            countries = [q_data[0][4], q_data[1][4], q_data[2][4], q_data[3][4]]

            max_pop_index = pops.index(max(pops))

            question.answer = f"{cities[max_pop_index]} ({countries[max_pop_index]})"

            question.options = []
            for i in range(4):
                question.options.append(f"{q_data[i][0]} ({q_data[i][4]})")
            
            question.text = f"Which of these has the highest population ({max(pops):,d}) ?\n".replace(","," ")

        print(question.text)

        random.shuffle(question.options)     

        # print the options for the question
        for i, option in enumerate(question.options,1):
            print(i, option, sep=". ", end="\n")

        # ask user for answer
        while True:
            try:
                answer_num = int(input("\nChoose your answer:"))
                if answer_num not in [1,2,3,4]:
                    print("Choose an answer between 1 and 4.")
                else:
                    break
            except:
                print("Choose an answer between 1 and 4.")
        
        # append question to list of questions for possible later use
        self.questions_list.append([question.text, question.options, question.answer])

        # get given answer from options
        answer_given = question.options[answer_num-1]

        # compare given answer to the original answer
        if(answer_given == question.answer):

            print("\nCorrect!")
            self.score += 1
            return True

        elif answer_given != question.answer :

            print(f"\nWrong. Correct answer was {question.answer}.")
            return False
        
    def getDistanceFromLatLng(self, lat1, lng1, lat2, lng2, miles=False):
        ''' 
        Method to calculate distance between two lat/lng coordinates in km using the Haversine formula
        There's also haversine library in PyPi, but decided not to include any libraries that you have to install separately
        '''
        r=6371 # radius of the earth in km
        lat1=radians(lat1)
        lat2=radians(lat2)
        lat_dif=lat2-lat1
        lng_dif=radians(lng2-lng1)
        a=sin(lat_dif/2.0)**2+cos(lat1)*cos(lat2)*sin(lng_dif/2.0)**2
        d=2*r*asin(sqrt(a))
        if miles:
            return d * 0.621371 # return miles
        else:
            return d # return km
        # Copyright 2016, Chris Youderian, SimpleMaps, http://simplemaps.com/resources/location-distance
        # Released under MIT license - https://opensource.org/licenses/MIT
        
class Question:
    '''Class to handle questions'''
    def __init__(self, type, text, options, answer):
        self.type = type
        self.text = text
        self.options = options
        self.answer = answer