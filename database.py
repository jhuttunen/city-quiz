import sqlite3, os

class Database():
    '''Class to handle database connection and queries'''

    def create_connection(self, db_file):
        """Method to create db connection"""

        conn = None
        try:
            conn = sqlite3.connect(db_file)
        except sqlite3.Error as e:
            print(e)

        return conn

    def get_random_cities(self, count, type, CONFIG):
        '''Method to get random cities from database'''

        conn = self.create_connection(os.path.join(os.path.dirname(os.path.realpath(__file__)),"cities.db"))
        cur = conn.cursor()
        
        # different questions possible if all countries selected in the settings
        if(CONFIG.selected_countries == "all"):
            if(type == "distances"):
                cur.execute("SELECT * FROM cities WHERE id IN (SELECT id FROM cities WHERE capital='primary' ORDER BY RANDOM() LIMIT ?)", (count,))
            elif(type == "capital"):
                cur.execute("SELECT * FROM cities WHERE id IN (SELECT id FROM cities WHERE capital='primary' ORDER BY RANDOM() LIMIT ?)", (count,))
            elif(type == "population"):
                cur.execute("SELECT * FROM cities WHERE id IN (SELECT id FROM cities WHERE capital='primary' and population > 0 ORDER BY RANDOM() LIMIT ?)", (count,))
            elif(type == "highest_population"):
                cur.execute("SELECT * FROM cities WHERE id IN (SELECT id FROM cities WHERE capital='primary' and population > 0 ORDER BY RANDOM() LIMIT ?)", (count,))

        # for specific country, remove question about capital cities
        else:
            country = CONFIG.selected_countries
            if(type == "distances"):
                cur.execute("SELECT * FROM cities WHERE id IN (SELECT id FROM cities WHERE iso3=? ORDER BY RANDOM() LIMIT ?)", (country, count,))
            elif(type == "population"):
                cur.execute("SELECT * FROM cities WHERE id IN (SELECT id FROM cities WHERE iso3=? and population > 0 ORDER BY RANDOM() LIMIT ?)", (country, count,))
            elif(type == "highest_population"):
                cur.execute("SELECT * FROM cities WHERE id IN (SELECT id FROM cities WHERE iso3=? and population > 0 ORDER BY RANDOM() LIMIT ?)", (country, count,))
        
        cities = cur.fetchall()

        return(cities)

    def get_country_codes(self):
        '''Method to get list of country codes from db'''

        conn = self.create_connection(os.path.join(os.path.dirname(os.path.realpath(__file__)),"cities.db"))
        cur = conn.cursor()
        cur.execute("SELECT DISTINCT iso3 FROM cities ORDER BY iso2 ASC")

        country_codes = []
        for c in cur.fetchall():
            country_codes.append(c[0])

        return country_codes
