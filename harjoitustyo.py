import os
from config import Config
from database import Database
from quiz import Quiz

def clear_screen():
    '''Function to clear terminal screen'''

    if os.name == 'nt':
        clear = lambda: os.system('cls') # Windows
        clear()
    else:
        os.system('clear') # Linux

def print_menu():
    '''Function to print starting menu'''

    # clear the screen, so it's not so messy
    clear_screen()

    # print current game settings
    CONFIG.print_config()

    menu_options = {
        1: "Start new game",
        2: "Edit settings",
        3: "Exit",
    }

    # print the menu
    for k in menu_options.keys():
        print(k, ": ", menu_options[k])
        user_input = 0

    # ask user for an option
    while user_input != 3:
        
        try:
            user_input = int(input())
        except:
            print("Choose an option between 1 and 3.")

        # option 1 starts new game
        if user_input == 1:
            print("Starting new game.")
            start_game()

        # option 2 is settings menu
        elif( user_input == 2):
            
            # get list of available country codes and print them
            db = Database()
            country_codes = db.get_country_codes()

            print("*"*50)
            print("List of country country codes:")
            for c in country_codes:
                print(c, end=", ")
            print()
            print("*"*50)

            CONFIG.print_config()

            while True:
                # ask user to choose setting
                selected_countries = input("Select 'all' or use three letter country code to pick specific country: ")

                # check the user input
                if(selected_countries in country_codes or selected_countries == "all"):

                    save = CONFIG.save_config(selected_countries)

                    if(save is True):
                        print("Settings saved successfully.")
                        print_menu()
                        break
        
        # option 3 exits the game
        elif user_input == 3:

            print("Exiting the program.")
            exit()

        else:
            print("Choose an option between 1 and 3.")            

def start_game():
    '''Function to start the quiz'''

    quiz = Quiz(CONFIG)
    
    while True:
        round = quiz.round
        print(f"\nQuestion number {quiz.round}")

        quiz.new_question()

        score = f"{quiz.score}/{quiz.round}"
        
        quiz.round += 1

        if(round > 0):
            print(f"You have answered {score} questions right.")

        user_input = input("\nPress 'n' for next question or 'q' to return to menu.")
        
        if(user_input == "q"):
            print_menu()
            break
    else:
        main()

if __name__ == "__main__":
    global CONFIG
    CONFIG = Config()
    print_menu()
