# Ohjelmoinnin perusteiden harjoitustyö

Kyseessä on maailman kaupunkeihin liittyvä tietovisa, jossa käyttäjältä kysytään tällä hetkellä neljää erilaista monivalintakysymystä kaupunkitietokannasta satunnaisesti luoduista kysymyksistä:

* Mikä on valtion x pääkaupunki?
* Mikä on kaupungin x väkiluku?
* Mikä kaupunki on x km etäisyydellä kolmesta muusta kaupungista?
* Minkä vaihtoehtojen kaupungeista väkiluku on isoin?

## Luokat ja tiedostot

Ohjelma ajetaan harjoitustyo.py -tiedostosta.

Luokat on jaettu kolmeen eri tiedostoon:
* config.py sisältää Config -luokan, jolla hallitaan asetuksia
* database.py sisältäää Database -luokan, jolla käsitellään tietokantayhteyksiä ja tietokantahakuja
* quiz.py isältää Quiz -luokan sekä Question -luokan, joissa käsitellään tietovisan kysymyksiä

## Käytetyt kirjastot

Tietokantana käytetään SQLiteen luotua cities.db tiedostoa, johon vein tiedot https://simplemaps.com -sivustolta ladatusta csv-tiedostosta.

Asetukset olisi ollut myös mahdollista ja kenties järkevämpää tallentaa tietokantaan, mutta halusin kokeilla myös tekstitiedostojen käsittelyä ja ConfigParseria asetusten hallintaan.

Muita hyödynnettäviä kirjastoja ovat os tiedostojenhallinnassa ja random satunnaisuusominauuksissa.

## Laajennettavuus

Ohjelmaa olisi mahdollista laajentaa merkittävästi esimerkiksi seuraavanlaisilla ominaisuuksilla:

* luomalla selkeän päämäärän, nyt kysymyksiä vain kysytään kunnes käyttäjä haluaa lopettaa
* lisäämällä erilaisten kysymysten määrää nykyisestä neljästä
* luomalla kysymyksiin vaikeustasot
* lisäämällä jonkinlaisen timerin
* kehittämällä logiikkaa, jolla vaihtoehdot haetaan.. nyt osa kysymyksistä hyvin vaikeita
* lisäämällä jonkinlaisen timerin, johon saisi vaikka lisäaikaa oikeilla vastauksilla
* antamalla käyttäjälle mahdollisuus tallentaa ja palata aiemman visan pariin
* luomalla "lifelinet" "Haluatko miljonääriksi" -tyyliin
* luomalla graafisen käyttöliittymän tai internetissä toimivan version
* graafiseen versioon voisi hyödyntää karttaelementtejä tai ahkea sijainnin perusteella esim. jostain wiki -lähteestä kuvia
* keräämällä tilastoja vastauksisista ja näyttämällä näistä käyttäjälle tietoja
* lisäämällä esimerkiksi käännöstiedoston, josta haetaan asetusten perusteella oikeankieliset tekstit

## Kaupunkitietokannan lisenssitiedot

Basic World Cities Database: The Provider offers a Basic World Cities Database free of charge. This database is licensed under the Creative Commons Attribution 4.0 license as described at: https://creativecommons.org/licenses/by/4.0/.
