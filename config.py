import configparser, os

class Config:
    '''Class to handle config settings'''

    def __init__(self):
        self.config
        self.selected_countries

    config_folder = os.path.dirname(os.path.realpath(__file__))
    settings_file = "settings.conf"
    full_config_file_path = os.path.join(config_folder, settings_file)
    
    # load configparser library 
    config = configparser.ConfigParser()
    
    # settings to use for first program run
    config["DEFAULT"] = {"selected_countries" : "all"}
    config["User"] = {"selected_countries" : "all"}
    
    # if settings.conf file does not exist, create one
    if not os.path.exists(full_config_file_path) or os.stat(full_config_file_path).st_size == 0:
        with open(full_config_file_path, "w") as configfile:
            config.write(configfile)
    
    # read settings from settings.conf
    config.read(full_config_file_path)

    # assign settings to variable
    selected_countries = config["User"]["selected_countries"]

    def print_config(self):
        '''Method to print current settings'''

        self.config.read(self.full_config_file_path)
        self.selected_countries = self.selected_countries = self.config["User"]["selected_countries"]

        print("SETTINGS currently selected countries: ", self.selected_countries)

    def save_config(self, value):
        '''Method to save new config settings(s)'''

        self.config["User"] = {"selected_countries" : value}

        try:
            # write new config settings to file
            with open(self.full_config_file_path, "w") as configfile:
                self.config.write(configfile)
            return True
        except:
            "Error saving settings."
            return False